module.exports = {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400'
      },]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
    '@nuxtjs/google-analytics',
  ],

  googleAnalytics: {
    id: 'UA-167439626-1'
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    '@nuxtjs/firebase'
  ],


  firebase: {
    config: {
      production: {
        apiKey: 'AIzaSyDCDdufCY1q8U8rJvJg0pdTa7nWQAmT_e8',
        authDomain: 'rinkis-birzus.web.app',
        databaseURL: 'https://rinkis-birzus.firebaseio.com/',
        projectId: 'rinkis-birzus',
        storageBucket: 'gs://rinkis-birzus.appspot.com',
      },
      development: {
        apiKey: 'AIzaSyDCDdufCY1q8U8rJvJg0pdTa7nWQAmT_e8',
        authDomain: 'rinkis-birzus.web.app',
        databaseURL: 'https://rinkis-birzus.firebaseio.com/',
        projectId: 'rinkis-birzus',
        storageBucket: 'gs://rinkis-birzus.appspot.com',
      }
    },
    services: {
      auth: { 
        ssr: {
          serverLogin: true
        }
      },
      firestore: true,
      storage: true,
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      config.module.rules.push({
          enforce : 'pre',
          test    : /\.(js|vue)$/,
          loader  : 'eslint-loader',
          exclude : /(node_modules)/,
          options : {
              fix : true
          }
      });

    }
  }
}
