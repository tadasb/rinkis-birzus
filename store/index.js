import { vuexfireMutations, firestoreAction } from 'vuexfire'

export const state = () => ({
  rentables: [],
  inactive_rentables: [],
  email: null,
  admin: false
})

export const mutations = {
  ...vuexfireMutations,
  setUser(state, user) {
    if (user) {
      state.email = user.email
    } else {
      state.email = null
    }
  },
  toogleAdmin(state) {
    state.admin = !state.admin
  }
}

export const actions = {
  setUser(context, { authUser }) {
    this.commit('setUser', authUser)
  },
  toogleAdmin(context) {
    this.commit('toogleAdmin')
  },

  bindRentablesRef: firestoreAction(async function({ bindFirestoreRef }) {
    const ref = this.$fireStore
      .collection('rentables')
      .where('active', '==', true)
    await bindFirestoreRef('rentables', ref, { wait: true })
  }),
  bindInactiveRentablesRef: firestoreAction(async function({
    bindFirestoreRef
  }) {
    const ref = this.$fireStore
      .collection('rentables')
      .where('active', '==', false)
    await bindFirestoreRef('inactive_rentables', ref, { wait: true })
  })
}
