import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'

if (!firebase.apps.length) {
  const config = {
    apiKey: 'AIzaSyDCDdufCY1q8U8rJvJg0pdTa7nWQAmT_e8',
    authDomain: 'rinkis-birzus.firebaseapp.com',
    databaseURL: 'https://rinkis-birzus.firebaseio.com',
    projectId: 'rinkis-birzus',
    storageBucket: 'rinkis-birzus.appspot.com'
  }
  firebase.initializeApp(config)
}

export default function({ store, redirect }) {
  return firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      store.dispatch('users/setCurrent', user)
      store.dispatch('accounts/setCurrent', user.uid)
    } else {
      redirect('/prisijungti')
    }
  })
}

export const db = firebase.firestore()
export const auth = firebase.auth()
export const storage = firebase.storage().ref()
